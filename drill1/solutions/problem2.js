import { inventory } from "./inventory.js";

//function to get the last car of the inventory
export function getLastCar(inventory) {
  let length = inventory.length;

  let lastCar = inventory[length - 1];

  return(`Last car is a ${lastCar["car_make"]} ${lastCar["car_model"]}`);
}

getLastCar(inventory);
