import { inventory } from "./inventory.js";

//function to get a list of selected cars from the inventory.
export function getSelectedCars(inventory, listOfCars) {
    let selectedCars = [];
    for (let car of inventory) {
        let carName = car["car_make"];
        if (listOfCars.includes(carName)) {
            selectedCars.push(car);
        }
    }
    return selectedCars;
}

let listOfCars = ["BMW", "Audi"];
let selectedCars = getSelectedCars(inventory, listOfCars);
let selectedCarsInString=(JSON.stringify(selectedCars));
