import { inventory } from "../../drill1/solutions/inventory.js"


export function getCarbyID(inventory, id) {
    const car = inventory.filter((car) => car.id == id);
    return `Car ${id} is a ${car[0].car_year} ${car[0]["car_make"]} ${car[0]["car_model"]}`;;
}

// console.log(getCarbyID(inventory,33));