import { inventory } from "../../drill1/solutions/inventory.js";

export function getLastCar(inventory) {
    let lastIndex = inventory.length - 1;
    const lastCar = inventory.filter((inventory, index, object) => index == lastIndex)
    return `Last car is a ${lastCar[0]["car_make"]} ${lastCar[0]["car_model"]}`;
}

// console.log(getLastCar(inventory));