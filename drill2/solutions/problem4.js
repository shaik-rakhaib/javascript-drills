import { inventory } from "../../drill1/solutions/inventory.js";

export function getAllYears(cars = inventory) {
    const years = cars.map(car => car.car_year);
    return years;
}

getAllYears(inventory);