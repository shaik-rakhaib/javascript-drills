import { inventory } from "../../drill1/solutions/inventory.js";

export function getSelectedCars(inventory) {
    const output = inventory.filter((car) => car.car_make == "Audi" || car.car_make == "BMW")
    return output;
}

// console.log(JSON.stringify(getSelectedCars(inventory)));