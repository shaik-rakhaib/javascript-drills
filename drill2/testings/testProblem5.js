import { customCars } from "../customTestCase.js";
import { getAllYears } from "../solutions/problem4.js";
import { getCount } from "../solutions/problem5.js";

const allYears = getAllYears(customCars)

const count = getCount(allYears, 2000);

console.log(count);

